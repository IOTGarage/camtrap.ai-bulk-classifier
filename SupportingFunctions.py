
from PIL import Image, ImageDraw, ImageFont
from math import floor
import pandas as pd
import numpy as np
global BASE_PATH
BASE_PATH = ""
def GetInputList():
    global BASE_PATH
    FileLimit=47695
    TotalImages=100
    stage = open("bulk_csv.csv", "r").read()
    if stage=="":
        stageFile=0
        stage = 0
    else:
        stage = int(stage)
        stageFile = str(floor(stage/FileLimit)+1)
    TDF = pd.read_csv(BASE_PATH+"MS_species_1.csv", index_col=0)
    try:
        DF = TDF.iloc[stage:stage+TotalImages-1].copy()
    except:
        DF = TDF.iloc[stage:FileLimit].copy()
    DF['path']=DF["path"].str.split("/").str[-1]
    DF["species_name"] = DF["species_name"].str.replace("\"", "")
    DF["fullname"] = DF["species_name"]+"("+DF['general_name']+")"
    del DF["species_name"], DF['general_name']
    return DF
def HashImage(Path):
    try:
        with Image.open(Path) as img:
            img = img.resize((8, 8), Image.ANTIALIAS)
            img = img.convert('L')
            pixels = list(img.getdata())
            avg_pixel = sum(pixels) / len(pixels)
            bits = "".join(['1' if pixel >= avg_pixel else '0' for pixel in pixels])
            hash_value = hex(int(bits, 2))[2:]
            hash_value = hash_value.zfill(64)
            return hash_value
    except Exception as e:
        print("Error:", e)
        return False
def DrawBox(image,
                              ymin,
                              xmin,
                              ymax,
                              xmax,
                              clss=None,
                              thickness=4,
                              expansion=0,
                              display_str_list=(),
                              use_normalized_coordinates=True,
                              label_font_size=16,
                              textalign=0):
  image = Image.open("static/images/animal1.png")
  """
  Adds a bounding box to an image.

  Bounding box coordinates can be specified in either absolute (pixel) or
  normalized coordinates by setting the use_normalized_coordinates argument.

  Each string in display_str_list is displayed on a separate line above the
  bounding box in black text on a rectangle filled with the input 'color'.
  If the top of the bounding box extends to the edge of the image, the strings
  are displayed below the bounding box.

  Args:
  image: a PIL.Image object.
  ymin: ymin of bounding box - upper left.
  xmin: xmin of bounding box.
  ymax: ymax of bounding box.
  xmax: xmax of bounding box.
  clss: str, the class of the object in this bounding box - will be cast to an int.
  thickness: line thickness. Default value is 4.
  expansion: number of pixels to expand bounding boxes on each side.  Default is 0.
  display_str_list: list of strings to display in box
      (each to be shown on its own line).
      use_normalized_coordinates: If True (default), treat coordinates
      ymin, xmin, ymax, xmax as relative to the image.  Otherwise treat
      coordinates as absolute.
  label_font_size: font size to attempt to load arial.ttf with
  """

  #Setting default colors for bounding boxes
  DEFAULT_COLORS = [
      'AliceBlue', 'Red', 'RoyalBlue', 'Gold', 'Chartreuse', 'Aqua', 'Azure',
      'Beige', 'Bisque', 'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue',
      'AntiqueWhite', 'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson',
      'Cyan', 'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
      'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
      'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
      'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'GoldenRod',
      'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
      'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
      'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
      'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
      'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
      'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
      'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
      'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
      'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
      'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
      'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
      'RosyBrown', 'Aquamarine', 'SaddleBrown', 'Green', 'SandyBrown',
      'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
      'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
      'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
      'WhiteSmoke', 'Yellow', 'YellowGreen'
  ]
  TEXTALIGN_LEFT = 0
  TEXTALIGN_RIGHT = 1
  colormap=DEFAULT_COLORS

  if clss is None:
      color = colormap[1]
  else:
      color = colormap[int(clss) % len(colormap)]

  draw = ImageDraw.Draw(image)
  im_width, im_height = image.size
  if use_normalized_coordinates:
      (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                    ymin * im_height, ymax * im_height)
  else:
      (left, right, top, bottom) = (xmin, xmax, ymin, ymax)

  if expansion > 0:

      left -= expansion
      right += expansion
      top -= expansion
      bottom += expansion

      # Deliberately trimming to the width of the image only in the case where
      # box expansion is turned on.  There's not an obvious correct behavior here,
      # but the thinking is that if the caller provided an out-of-range bounding
      # box, they meant to do that, but at least in the eyes of the person writing
      # this comment, if you expand a box for visualization reasons, you don't want
      # to end up with part of a box.
      #
      # A slightly more sophisticated might check whether it was in fact the expansion
      # that made this box larger than the image, but this is the case 99.999% of the time
      # here, so that doesn't seem necessary.
      left = max(left,0); right = max(right,0)
      top = max(top,0); bottom = max(bottom,0)

      left = min(left,im_width-1); right = min(right,im_width-1)
      top = min(top,im_height-1); bottom = min(bottom,im_height-1)

  # ...if we need to expand boxes

  draw.line([(left, top), (left, bottom), (right, bottom),
              (right, top), (left, top)], width=thickness, fill=color)

  try:
      font = ImageFont.truetype('arial.ttf', label_font_size)
  except IOError:
      font = ImageFont.load_default()

  # If the total height of the display strings added to the top of the bounding
  # box exceeds the top of the image, stack the strings below the bounding box
  # instead of above.
  display_str_heights = [font.getsize(ds)[1] for ds in display_str_list]

  # Each display_str has a top and bottom margin of 0.05x.
  total_display_str_height = (1 + 2 * 0.05) * sum(display_str_heights)

  if top > total_display_str_height:
      text_bottom = top
  else:
      text_bottom = bottom + total_display_str_height

  # Reverse list and print from bottom to top.
  for display_str in display_str_list[::-1]:

      # Skip empty strings
      if len(display_str) == 0:
          continue

      text_width, text_height = font.getsize(display_str)

      text_left = left

      if textalign == TEXTALIGN_RIGHT:
          text_left = right - text_width

      margin = np.ceil(0.05 * text_height)

      draw.rectangle(
          [(text_left, text_bottom - text_height - 2 * margin), (text_left + text_width,
                                                            text_bottom)],
          fill=color)

      draw.text(
          (text_left + margin, text_bottom - text_height - margin),
          display_str,
          fill='black',
          font=font)

      text_bottom -= (text_height + 2 * margin)
  image.show()
  return(image)