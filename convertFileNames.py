import os
def rename_files(root_dirs):
    for root_dir in root_dirs:
        for sub_dir in range(20):
            for subsub_dir in range(50):
                path = os.path.join(root_dir, str(sub_dir), str(subsub_dir))
                if os.path.isdir(path):
                    for filename in os.listdir(path):
                        if filename.endswith('.jpg'):
                            root_num = root_dirs.index(root_dir)
                            new_filename = f"{root_num}{str(sub_dir).zfill(2)}{str(subsub_dir).zfill(2)}{filename.split('.')[0].zfill(3)}.jpg"
                            old_filepath = os.path.join(path, filename)
                            new_filepath = os.path.join(path, new_filename)
                            os.rename(old_filepath, new_filepath)
                            print (old_filepath, new_filepath)
root_directories = ['0', '1', '2', '3']
rename_files(root_directories)
