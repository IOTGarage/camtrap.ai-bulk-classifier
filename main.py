from flask import Flask, render_template, jsonify, request, redirect, url_for, send_from_directory
from SupportingFunctions import DrawBox, GetInputList, HashImage
import os
import inspect
import pandas as pd
global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, ALLOWED_EXTENSIONS, boxedpath_directory
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
BASE_PATH = "/mnt/e/camtrap/"
BC_FOLDER = 'bulkClassifier/'
boxedpath_directory = "static/images/boxed/"
def UpdateStage():
    stage = open("bulk_csv.csv", "r")
    currentstage = int(stage.read())
    print ("Current Stage: ", currentstage)
    stage.close()
    stage = open("bulk_csv.csv", "w")
    newStage = str(currentstage+100)
    print ("New Stage:", newStage)
    stage.write(newStage)
    stage.close()
    stage = open("bulk_csv.csv", "r")
    oldstage = int(stage.read())
    if (oldstage==int(newStage)):
        print ("All Good")
    else:
        print ("Stage not updated, need troubleshooting")
SessionData = {}
ImagePath = 'static/images/bulkClassifier/'
BoxImagePath = 'static/images/boxed/'
app = Flask(__name__)
@app.route('/')
def index():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    return render_template('index4.html', Stage=0)
@app.route('/animal_images_api')
def get_animal_images():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    animal_name = request.args.get('animal')
    return jsonify(animal_images_data.get(animal_name, []))
@app.route('/animal_buttons_api')
def get_animal_buttons():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    print ("animal buttons function executed")
    cf = inspect.stack()[1]
    cfn = cf.function
    cfin = cf.filename
    cln = cf.lineno
    print (cfn, cfin, cln)
    InputFiles = GetInputList()
    boxedpath_directory = "boxedImage/"
    Stage = str(int(open("bulk_csv.csv", "r").read()))
    UpdateStage()
    animal_buttons_data = []
    for a in list(InputFiles["fullname"].unique()):
        NewDict={"name":a, "Stage":Stage}
        animal_buttons_data.append(NewDict)
    animal_images_data = {}
    for animal in animal_buttons_data:
        animal_images_data[animal['name']]=[]
        for index, row in InputFiles[InputFiles["fullname"]==animal["name"]].iterrows():
            path = row[0].split("/")[-1]
            conf = row[2]
            BC_FOLDER = 'bulkClassifier/'
            uniq_number = path.split('.')[0].split('_')[-1]
            box_pp = "anno_~var~home~akin~pics~"+uniq_number+"~"+uniq_number+"~"
            animal_images_data[animal['name']].append([BC_FOLDER+path, conf, boxedpath_directory+box_pp+path])
    return jsonify(animal_buttons_data)
@app.route('/store-data', methods=['POST'])
def store_data():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    data = request.json
    BC_FOLDER = 'bulkClassifier/'
    Stage = data[0]['Stage']
    resultsFile = open('static/content/results_'+Stage+'.csv', 'a')
    newbox = data[0]["classification"]
    if (newbox not in animal_buttons_data):
        animal_buttons_data.append(newbox)
        animal_images_data[newbox]=[]
    for item in data:
        newname = item['imageName']
        try:
            newname = newname.split("/")[-1]
        except:
            pass
        line = newname+","+newbox+"\n"
        uniq_number = newname.split('.')[0].split('_')[-1]
        box_pp = "anno_~var~home~akin~pics~"+uniq_number+"~"+uniq_number+"~"
        animal_images_data[newbox].append([BC_FOLDER+newname, 100, boxedpath_directory+box_pp+newname])
        resultsFile.write(line)
    resultsFile.close()
    return jsonify({'message': 'Data stored successfully'}), 200
@app.route('/newSession', methods=['POST'])
def newSession():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    Stage=request.json
    try:
        resultsFile = open('static/content/results_'+Stage+'.csv', 'r')
        ClassifiedImages = resultsFile.read().split("\n")
        resultsFile.close()
    except:
        ClassifiedImages = []
    undoneImages=[]
    for index, row in InputFiles.iterrows():
        undone=1
        for ci in ClassifiedImages:
            if (ci.split(",")[0].split("/")[-1]==row["path"].split("/")[-1]):
                undone=0
        if (undone):
            undoneImages.append(row["path"])
    undrecord = open("NonHumanClassifiedImages.csv", "a")
    resultsFileUpdate = open('static/content/results_'+Stage+'.csv', 'a')
    for und in undoneImages:
        undrecord.write(und+"\n")
        resultsFileUpdate.write(und+",not done\n")
    resultsFileUpdate.close()
    undrecord.close()
    #return get_animal_buttons()
    return jsonify({'message': 'Session concluded successfully'}), 200
@app.route('/exitSystem', methods=['POST'])
def exitSystem():
    global InputFiles, animal_buttons_data, animal_images_data, viaUpload, BulkClassifier, BC_FOLDER
    global ImagePath, BoxImagePath, SessionData, LeftOvers, Stage, BASE_PATH, boxedpath_directory
    Stage = request.json
    try:
        resultsFile = open('static/content/results_'+Stage+'.csv', 'r')
        ClassifiedImages = resultsFile.read().split("\n")
        resultsFile.close()
    except:
        ClassifiedImages = []
    undoneImages=[]
    Route = request.json.split("/")[-1]
    for index, row in InputFiles.iterrows():
        undone=1
        for ci in ClassifiedImages:
            if (ci.split(",")[0].split("/")[-1]==row["path"].split("/")[-1]):
                undone=0
        if (undone):
            undoneImages.append(row["path"])
    undrecord = open("NonHumanClassifiedImages.csv", "a")
    resultsFileUpdate = open('static/content/results_'+Stage+'.csv', 'a')
    for und in undoneImages:
        undrecord.write(und+"\n")
        resultsFileUpdate.write(und+",not done\n")
    resultsFileUpdate.close()
    undrecord.close()
    return jsonify({'message': 'Exit Successful'}), 200
@app.route('/bulkClassifier/<path:filename>')
def getAnimalImage(filename):
    directory = "/mnt/e/camtrap/uploaded_pics/"
    if os.path.exists(directory+filename):
        return send_from_directory(directory, filename)
    else:
        # directory = "/mnt/e/camtrap/Images/"
        # return send_from_directory(directory, filename)
        directory = "static/content/"
        return send_from_directory(directory, "noboxfile.jpg")
@app.route('/boxedImage/<path:filename>')
def getBoxedImage(filename):
    directory = "/mnt/e/camtrap/sep_pics/1/with_bb/"
    if os.path.exists(directory+filename):
        return send_from_directory(directory, filename)
    else:
        directory = "/mnt/c/Users/User/OneDrive/dgfc_code/static/content/"
        return send_from_directory(directory, "noboxfile.jpg")
@app.route('/Instructions')
def InstructionsVideo():
    return send_from_directory("static/", "camtrap_instructionVideo.mp4")
if __name__ == '__main__':
    app.run(debug=True, port=5000, host='127.0.0.1')