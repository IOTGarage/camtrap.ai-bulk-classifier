var popupEnabled=1;
var instructions=1;
var AnimalSelected=false;
var naImage, emptyImage, human;
var Stage;
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}
var NamesData;
readTextFile("static/content/suggestions.json", function(text){
    NamesData = JSON.parse(text);
});

function hideSelectedImages() {
    const selectedImages = document.querySelectorAll('.highlighted');
    selectedImages.forEach(image => {
        image.style.display = 'none';
    });
	const selectedImages2 = document.querySelectorAll('.highlighted_part');
    selectedImages2.forEach(image => {
        image.style.display = 'none';
    });
}
const resetButton = document.getElementById('resetButton');
    resetButton.addEventListener('click', function() {
    document.getElementById("okButton").disabled=true;
    document.getElementById("resetButton").disabled=true;
	toggleRadios(true);
    resetHighlightedImages();
    popupEnabled=1;
});
function moveImages(){
    hideSelectedImages();
    popupEnabled=1;
    document.getElementById("okButton").disabled=true;
    document.getElementById("resetButton").disabled=true;
    var frm = document.getElementById('moveTo');
    var result;
    if (frm.elements["action"].value=="other"){
        result = document.getElementById("nameInput").value;
    }
    else{
        result = frm.elements["action"].value;
    }
    updateClassifications(result);
    resetHighlightedImages();
    toggleRadios(true);
}
function Load100Images(){
	loadAnimalButtons();
    toggleRadios(true);
	document.getElementById("animal_images").innerHTML="Click on the animal name to receive images.";
	document.getElementById("load100Button").style.display="none";
	document.getElementById("nextButton").style.display="inline";
}
window.onload = function() {
    var introwidth = document.getElementById("second_div").clientHeight;
    document.getElementById("hideableDiv").style.height=introwidth+"px";
    document.getElementById("first_div").style.height=introwidth+"px";
    document.getElementById("third_div").style.height=introwidth+"px";
    };
function toggleDiv() {
    var hideableDiv = document.getElementById("hideableDiv");
    if (instructions){
        hideableDiv.style.display='none';
        instructions = 0
        document.getElementById("infoButton").innerHTML="Show Instructions"
    }
    else{
        hideableDiv.style.display='block';
        instructions=1;
        document.getElementById("infoButton").innerHTML="Hide Instructions"
    }
}
document.getElementById("naButton").addEventListener("click", function() {
    emptyImage = 0;
    naImage = 1;
});
document.getElementById("emptyButton").addEventListener("click", function() {
    emptyImage = 1;
    naImage = 0;
});
function loadAnimalButtons() {
    fetch('/animal_buttons_api')
        .then(response => response.json())
        .then(data => {
            document.getElementById("animal_buttons").innerHTML = "";
            data.forEach(animal => {
                var button = document.createElement("button");
				document.getElementById("stage-number").innerHTML=animal.Stage;
                button.innerHTML = animal.name;
                button.style.fontSize="14px";
                button.style.marginRight="2.5px";
                button.style.marginLeft="2.5px";
                button.style.marginTop = "2.5px";
                button.style.marginBottom = "2.5px";
                button.classList.add("animalButtonz");
                button.addEventListener('click', function() {
                    button.classList.add("clicked");
                    fetchAnimalImages(animal.name);
                });
                document.getElementById("animal_buttons").appendChild(button);
            });
        })
        .catch(error => console.error('Error fetching AnimalName buttons:', error));
}
var popupclear=1;
function fetchAnimalImages(animalName) {
popupEnabled=1;
var totalAnimalButtons = document.querySelectorAll('.animalButtonz');
var clickedButtons = document.querySelectorAll('.clicked');
if (totalAnimalButtons.length == clickedButtons.length){
    document.getElementById("nextButton").disabled=false;
}
fetch('/animal_images_api?animal=' + animalName)
    .then(response => response.json())
    .then(data => {
        const header = document.getElementById('fixedops');
        const imagesinaLine = Math.floor(header.offsetWidth/330);
        const linebreak = document.createElement("div");
        linebreak.style.clear="both";
        var imgNum = 0;
        document.getElementById("animal_images").innerHTML = "";
		const leftrightMargin = header.offsetWidth - (imagesinaLine*330);
		if (leftrightMargin>30){
			document.getElementById("animal_images").style.marginLeft=leftrightMargin/2+"px";
		}
		else{
			document.getElementById("animal_images").style.marginLeft="10px";
		}
        data.forEach(imageFileName => {
            var conf = imageFileName[1];
            var boxedpath = imageFileName[2];
            imageFileName = imageFileName[0];
            var img_div = document.createElement("div");
            var img = document.createElement("img");
            img.setAttribute("id", imageFileName);
            var conf_text = document.createElement("span");
            conf_text.style.position="relative";
            conf_text.style.backgroundColor="red";
            conf_text.style.color="white";
			if (conf<1){
				conf_text.innerHTML="Confidence: "+(conf*100).toFixed(2)+"%";
			}
			else{
				conf_text.innerHTML="Confidence: "+conf+"%";
			}
            conf_text.style.left="0%";
            conf_text.style.top="-99%";
            img.src = imageFileName;
            img.alt = imageFileName;
            img_div.style.width = "320px";
            img_div.style.height = "240px";
            img_div.style.marginRight = "10px";
            img_div.style.marginTop = "10px";
            img.style.width="100%";
            img.style.height="100%";
            document.getElementById("nameInput").value=animalName;
            var animalchars = animalName.length * 10;
            document.getElementById("nameInput").style.minWidth="100px";
            document.getElementById("nameInput").style.maxWidth="500px";
            document.getElementById("nameInput").style.width=animalchars.toString()+"px";
            document.getElementById("selectAllButton").disabled=false;
            img.ondblclick = function(e) {
                if (popupclear==1 && (!e.ctrlKey || e.metaKey)){
                    openImagePopupHover(img.src, conf, boxedpath);
                }
            };
            img.onclick = function(e) {
                if (e.ctrlKey || e.metaKey) {
                    if (img.classList.contains('highlighted')){
                        img.classList.remove('highlighted');
						img_div.classList.remove('highlighted_part');
                        const highlightedImages = document.querySelectorAll('.highlighted');
                        if (highlightedImages.length==0){
                            document.getElementById("okButton").disabled=true;
                            document.getElementById("resetButton").disabled=true;
                            toggleRadios(true);
                        }
                    }
                    else{
                        img.classList.add('highlighted');
						img_div.classList.add('highlighted_part');
                        document.getElementById("okButton").disabled=false;
                        document.getElementById("resetButton").disabled=false;
                        toggleRadios(false);
                    }
                }
            }; 
            document.getElementById("animal_images").appendChild(img_div);
            img_div.appendChild(img);
            img_div.appendChild(conf_text);
			imgNum+=1;
            if (imgNum<imagesinaLine){
                img_div.style.float="left";
                imgNum=0;
            }
            else{
                img_div.style.clear="both";
            }
        });
    })
    .catch(error => console.error('Error fetching image file names:', error));
}
function openImagePopupHover(imageSrc, conf, boxedpath) {
    if (popupclear==1){
        popupclear=0;
        const popup = document.getElementById('popup');
        const zoomImage = document.getElementById('zoomImage');
        const header = document.getElementById('fixedops');
        const showBoxButton = document.getElementById("showBoxButton");
        const closeButton = document.getElementById('closeButton');
        const popup_conf = document.getElementById("popup_conf");
		if (conf<1){
				popup_conf.innerHTML="Confidence: "+(conf*100).toFixed(2)+"%";
			}
			else{
				popup_conf.innerHTML="Confidence: "+conf+"%";
			}
		popup_conf.style.backgroundColor="red";
        zoomImage.src=imageSrc;
        const windowWidth = header.offsetWidth;
        const windowHeight = document.documentElement.scrollHeight;
		var boxed=0;
        function showBox() {
            if (boxed==0){
				zoomImage.src=boxedpath;
                showBoxButton.innerHTML="Hide Bounding Box";
				boxed=1;
            }
            else if(boxed==1){
                zoomImage.src=imageSrc;
                showBoxButton.innerHTML="Show Bounding Box";
				boxed=0;
            }
        }
		let zoomStage=0;
        function handleZoom(){
            if (zoomStage==0){
                popup.style.width="800px";
                popup.style.height="600px";
				zoomStage=1;
            }
            else if(zoomStage==1){
				popup.style.width="1024px";
                popup.style.height="720px";
				zoomStage=2;
            }
			else if(zoomStage==2){
				boxed=0;
				zoomStage=0;
                closePopup();
			}
        }
        function closePopup() {
                zoomImage.src="";
                popup.style.display = 'none';
                popupclear=1;
            }
        popup.style.width="640px";
        popup.style.height="480px";
        showBoxButton.style.display="block";
        showBoxButton.addEventListener('click', showBox);
        popup.style.display = 'block';
        closeButton.addEventListener('click', closePopup);
        zoomImage.addEventListener('click', handleZoom);
    }
}
function toggleRadios(task){
	AnimalSelected=!task;
    document.getElementById("humanButton").disabled=task;
    document.getElementById("emptyButton").disabled=task;
    document.getElementById("naButton").disabled=task;
    document.getElementById("othersButton").disabled=task;
}
function resetHighlightedImages() {
    const highlightedImages = document.querySelectorAll('.highlighted');
    highlightedImages.forEach(image => {
        image.classList.remove('highlighted');
    });
	const highlightedImages2 = document.querySelectorAll('.highlighted_part');
    highlightedImages2.forEach(image => {
        image.classList.remove('highlighted_part');
    });
}
function updateClassifications(classification) {
    const highlightedImages = document.querySelectorAll('.highlighted');
    const dataToSend = [];
	Stage = document.getElementById("stage-number").innerHTML;
    highlightedImages.forEach(image => {
        const imageName = image.getAttribute('src');
        dataToSend.push({imageName, classification, Stage});
    });
    fetch('/store-data', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataToSend)
    }).then(response => {
        if (response.ok) {
            console.log('Data sent successfully');
        } else {
            console.error('Failed to send data');
        }
    }).catch(error => {
        console.error('Error:', error);
    });
    var totalAnimalButtons = document.querySelectorAll('.animalButtonz');
    var animalnotexists = 1;
    if (totalAnimalButtons.length>0){
        totalAnimalButtons.forEach((btndata) => {
            if (classification==btndata.innerHTML){
                animalnotexists=0;
            }
        });
    }
    if (animalnotexists){
        var button = document.createElement("button");
        button.innerHTML = classification;
        button.style.fontSize="14px";
        button.style.marginRight="2.5px";
        button.style.marginLeft="2.5px";
        button.style.marginTop = "2.5px";
        button.style.marginBottom = "2.5px";
        button.classList.add("animalButtonz");
        button.addEventListener('click', function() {
            button.classList.add("clicked");
            document.getElementById("animal_images").innerHTML = "";
            fetchAnimalImages(classification);
        });
        document.getElementById("animal_buttons").appendChild(button);
    }
}
const disabledInput = document.getElementById('nameInput');
const popupForm = document.getElementById('popupForm');
const nameForm = document.getElementById('nameForm');
const suggestionsContainer = document.getElementById('suggestionsContainer');

function AddAnother()
{
    document.getElementById('leftInput2').style.display="block";
    document.getElementById('rightInput2').style.display="block";
    document.getElementById('addinputbutton').style.display="none";
}
// Get left and right input elements
const leftInput = document.getElementById('leftInput1');
const rightInput = document.getElementById('rightInput1');
const leftInput2 = document.getElementById('leftInput2');
const rightInput2 =document.getElementById('rightInput2');
// Get suggestions divs for left and right inputs
const leftSuggestionsDiv = document.getElementById('leftSuggestions');
const rightSuggestionsDiv = document.getElementById('rightSuggestions');
let pairCount = 1;

// Show popup form when disabled input is clicked
disabledInput.addEventListener('click', () => {
  if (AnimalSelected){
	  popupForm.style.display = 'block';
	  const considertwosets = disabledInput.value.split(",");
	  var array, leftVal, rightVal;
	  if (considertwosets.length>1){
		array = considertwosets[0].split("(");
		leftVal = array[0];
		rightVal = array[1].split(")")[0];
		leftInput.value=leftVal;
		rightInput.value=rightVal;
		array = considertwosets[1].split("(");
		leftVal = array[0];
		rightVal = array[1].split(")")[0];
		leftInput2.value=leftVal;
		rightInput2.value=rightVal;
		leftInput2.style.display="block";
		rightInput2.style.display="block";
	  }
	  else{
		  array = disabledInput.value.split("(");
		  leftVal = array[0];
		  rightVal = array[1].split(")")[0];
		  leftInput.value=leftVal;
		  rightInput.value=rightVal;
	  }
  }
});

// Close popup form
function closeForm() {
	document.getElementById('leftInput2').style.display="none";
    document.getElementById('rightInput2').style.display="none";
    document.getElementById('addinputbutton').style.display="block";
    popupForm.style.display = 'none';
  }
  function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

function showSuggestions(input, counterpart, suggestionsDiv, data, ref) {
    const inputValue = input.value.toLowerCase();
    var suggestions;
    if (ref=="left"){
        suggestions = Object.keys(data).filter(key => key.toLowerCase().startsWith(inputValue));
    }
    else{
        suggestions = Object.values(data).filter(value => value.toLowerCase().startsWith(inputValue));
    }
    suggestionsDiv.innerHTML = '';
    const limitedSuggestions = suggestions.slice(0, 5);
    limitedSuggestions.forEach(suggestion => {
      const div = document.createElement('div');
      div.textContent = suggestion;
      div.addEventListener('click', () => {
        input.value = suggestion;
        suggestionsDiv.innerHTML = '';
        if (ref=="left"){
            updateCounterpart(input, counterpart, NamesData, "key");
        }
        else{
            updateCounterpart(input, counterpart, NamesData, "value");
        }
      });
      suggestionsDiv.appendChild(div);
    });
  }
  function updateCounterpart(input, counterpartInput, data, keyOrValue) {
    const inputValue = input.value;
    var counterpartValue;
    if (keyOrValue=="key"){
        counterpartValue = data[inputValue];
    }
    else{
        counterpartValue = getKeyByValue(data, inputValue);
    }
    counterpartInput.value = counterpartValue;
  }
  // Event listener for left input
  leftInput.addEventListener('input', () => {
    showSuggestions(leftInput, rightInput, leftSuggestionsDiv, NamesData, "left");
  });
  // Event listener for right input
  rightInput.addEventListener('input', () => {
    showSuggestions(rightInput, leftInput, rightSuggestionsDiv, NamesData, "right");
  });

  // Event listener for left input
  leftInput2.addEventListener('input', () => {
    showSuggestions(leftInput2, rightInput2, leftSuggestionsDiv, NamesData, "left");
  });
  // Event listener for right input
  rightInput2.addEventListener('input', () => {
    showSuggestions(rightInput2, leftInput2, rightSuggestionsDiv, NamesData, "right");
  });

function SaveNames(){
    var newname = "";
    newname += leftInput.value;
    newname += "("+rightInput.value+")";
    if (leftInput2.value!=""){
        newname += ","+leftInput2.value
        newname += "("+rightInput2.value+")";
    }
    document.getElementById("nameInput").value=newname;
    closeForm();
  }
Stage = document.getElementById("stage-number").innerHTML;
const exitMessage = "<h1>System closed normally, please <a href='static/content/results_"+Stage+".csv'><strong>'click here'</strong></a> to download results or press <strong>'Restart',</strong> button to continue.</h1>";
function newSession(){
    const done = confirm("Are you sure everything is done correctly? If not press cancel and verify once more or press OK to proceed and save. You CANNOT come back to this after proceeding.");
    Stage = document.getElementById("stage-number").innerHTML;
	document.getElementById("nextButton").disabled=true;
	document.getElementById("nameInput").value="";
	if (done){
        fetch('/newSession', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(Stage)}).then(response => {
            if (response.ok) {
                    Load100Images();
            } else {
                console.error('Error generating report');
            }
        }).catch(error => {
            console.error('Error:', error);
        });
    }
}
function exitSystem(){
    if (document.getElementById("exitButton").innerHTML == "Restart"){
        document.getElementById("animal_images").innerHTML="<h1>Click on the animal name to receive images.</h1>";
        document.getElementById("exitButton").innerHTML = "Exit";
        location.reload();
    }
    else{
        document.getElementById("animal_buttons").innerHTML = "";
        document.getElementById("animal_images").innerHTML = exitMessage;
        Stage = document.getElementById("stage-number").innerHTML;
        fetch('/exitSystem', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(Stage)
        }).then(response => {
            if (response.ok) {
                console.log('Exit Successful');
            } else {
                console.error('Error generating report');
            }
        }).catch(error => {
            console.error('Error:', error);
        });
        document.getElementById("exitButton").innerHTML = "Restart";
    }
}
function selectAllImages(){
    const parentDiv = document.getElementById('animal_images');
    const images = document.querySelectorAll('img');
    const childDivs = parentDiv.querySelectorAll('div');
    images.forEach(image => {
        image.classList.add('highlighted');
    });
    childDivs.forEach(div => {
        div.classList.add('highlighted_part');
    });
    document.getElementById("okButton").disabled=false;
    document.getElementById("resetButton").disabled=false;
    toggleRadios(false);
}